package com.daydreamersteam.utility.utils

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.annotation.AnimRes
import androidx.annotation.AnimatorRes
import androidx.annotation.WorkerThread
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.location.LocationManagerCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.daydreamersteam.utility.R
import com.daydreamersteam.utility.dialogs.SimpleDialog
import com.google.gson.Gson
import org.jetbrains.anko.sdk27.coroutines.onClick
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

@WorkerThread
fun exportBitmapFromView(view: View): Bitmap {
    val bitmap =
        Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
    view.draw(Canvas(bitmap))
    return bitmap
}

@WorkerThread
fun saveBitmapToFile(bitmap: Bitmap, file: File): Boolean {
    return try {
        FileOutputStream(file).use { out ->
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
        }
        true
    } catch (e: IOException) {
        e.printStackTrace()
        false
    }
}

fun openSupportMail(
    context: Context,
    email: String,
    title: String = "",
    text: String = ""
) {
    val emailIntent = Intent(Intent.ACTION_SEND).apply {
        type = "plain/text"
        putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
        putExtra(Intent.EXTRA_SUBJECT, title)
        putExtra(Intent.EXTRA_TEXT, text)
        flags = Intent.FLAG_ACTIVITY_NEW_TASK
    }
    context.startActivity(
        Intent.createChooser(
            emailIntent,
            context.getString(R.string.select_mail_app)
        )
    )
}

fun openShareIntent(context: Context, text: String) {
    val shareIntent = Intent(Intent.ACTION_SEND).apply {
        type = "*/*"
        putExtra(
            Intent.EXTRA_TEXT,
            text
        )
        flags = Intent.FLAG_ACTIVITY_NEW_TASK
    }
    context.startActivity(Intent.createChooser(shareIntent, "Share to:"))
}

fun random(min: Int, max: Int) = Random().nextInt((max - min) + 1) + min
fun hasInternetConnection(context: Context): Boolean {
    val cm =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        cm.activeNetwork != null
    } else {
        cm.activeNetworkInfo != null && cm.activeNetworkInfo!!.isConnectedOrConnecting
    }
}

fun openAppPlayStore(context: Context) {
    try {
        context.startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("market://details?id=${context.packageName}")
            ).apply { flags = Intent.FLAG_ACTIVITY_NEW_TASK })
    } catch (e: ActivityNotFoundException) {
        context.startActivity(Intent(
            Intent.ACTION_VIEW,
            Uri.parse("https://play.google.com/store/apps/details?id=${context.packageName}")
        ).apply { flags = Intent.FLAG_ACTIVITY_NEW_TASK })
    }
}

fun getAppPlayStoreUrl(context: Context) =
    "https://play.google.com/store/apps/details?id=${context.packageName}"

fun openDaydreamersTeamStore(context: Context) {
    try {
        context.startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("market://developer?id=Daydreamers+Team")
            ).apply { flags = Intent.FLAG_ACTIVITY_NEW_TASK })
    } catch (e: ActivityNotFoundException) {
        context.startActivity(Intent(
            Intent.ACTION_VIEW,
            Uri.parse("https://play.google.com/store/apps/developer?id=Daydreamers+Team")
        ).apply { flags = Intent.FLAG_ACTIVITY_NEW_TASK })
    }
}

fun openWeb(context: Context, url: String) {
    val browserIntent =
        Intent(
            Intent.ACTION_VIEW,
            Uri.parse(url)
        )
    context.startActivity(browserIntent)
}

fun logd(tag: String, message: String) {
    Log.d(tag, message)
}

fun loge(tag: String, message: String) {
    Log.e(tag, message)
}

fun postDelayed(command: () -> Unit, duration: Long = 300) {
    Handler().postDelayed(command, duration)
}

fun Any?.isNull(): Boolean = this == null
fun Any?.isNotNull(): Boolean = this != null

fun <T> LiveData<T>.observeWithOldData(owner: LifecycleOwner, callback: (old: T?, new: T) -> Unit) {
    var old: T? = value
    observe(owner, Observer { new ->
        callback.invoke(old, new)
        old = new
    })
}

fun View.click(listener: (View) -> Unit) {
    onClick {
        listener.invoke(this@click)
        isClickable = false
        postDelayed({ isClickable = true }, 300)
    }
}

fun Fragment.removeSelf() {
    //this is wrong since we pop the first fragment, not the exact fragment we want to pop. but since the fragment we want to remove is always on top, so this is fine by now
    if (!requireFragmentManager().popBackStackImmediate()) {
        requireFragmentManager().beginTransactionWithAnim().remove(this).commit()
    }
}

fun createItemsDialog(
    context: Context,
    items: Array<String>,
    cancelable: Boolean,
    itemSelected: (Int) -> Boolean
) =
    AlertDialog.Builder(context, R.style.AlertDialog)
        .setItems(items) { dialog, which ->
            itemSelected.invoke(which)
        }.setCancelable(cancelable).create()

fun createSingleChoiceDialog(
    context: Context,
    items: Array<String>,
    selected: Int,
    cancelable: Boolean,
    itemSelected: (Int) -> Unit
) =
    AlertDialog.Builder(context, R.style.AlertDialog)
        .setSingleChoiceItems(
            items, selected
        ) { dialog, which ->
            dialog?.dismiss()
            itemSelected.invoke(which)
        }.setCancelable(cancelable).create()

fun createMultipleChoiceDialog(
    context: Context,
    items: Array<String>,
    selected: BooleanArray,
    cancelable: Boolean,
    itemSelected: (BooleanArray) -> Unit
): AlertDialog {
    val selectors = selected
    return AlertDialog.Builder(context, R.style.AlertDialog)
        .setMultiChoiceItems(
            items, selected
        ) { _: DialogInterface, which: Int, _selected: Boolean ->
            selectors[which] = _selected
        }.setCancelable(cancelable).setPositiveButton(R.string.ok) { dialog, which ->
            dialog.dismiss()
            itemSelected.invoke(selectors)
        }.setNegativeButton(
            R.string.cancel
        ) { dialog, _ ->
            dialog.dismiss()
        }.create()
}

fun createSimpleDialog(
    fragmentManager: FragmentManager,
    title: String?,
    content: String?,
    cancelable: Boolean,
    positiveText: String,
    positiveClick: () -> Unit,
    negativeText: String? = null,
    negativeClick: (() -> Unit)? = null
) = SimpleDialog.getInstance(
    title,
    content,
    cancelable,
    positiveText,
    positiveClick,
    negativeText,
    negativeClick
).show(
    fragmentManager,
    SimpleDialog.TAG
)

fun FragmentManager.beginTransactionWithAnim(
    @AnimatorRes @AnimRes enter: Int = android.R.animator.fade_in,
    @AnimatorRes @AnimRes exit: Int = android.R.animator.fade_out
) = beginTransaction().setCustomAnimations(enter, exit, enter, exit)

fun isReadPermissionGranted(context: Context) =
    ContextCompat.checkSelfPermission(
        context,
        Manifest.permission.READ_EXTERNAL_STORAGE
    ) == PackageManager.PERMISSION_GRANTED

fun requestReadPermission(activity: Activity, requestCode: Int) = ActivityCompat.requestPermissions(
    activity,
    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
    requestCode
)

fun isWritePermissionGranted(context: Context) =
    ContextCompat.checkSelfPermission(
        context,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    ) == PackageManager.PERMISSION_GRANTED

fun requestWritePermission(activity: Activity, requestCode: Int) =
    ActivityCompat.requestPermissions(
        activity,
        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
        requestCode
    )

fun isLocationPermissionGranted(context: Context) = ActivityCompat.checkSelfPermission(
    context,
    Manifest.permission.ACCESS_FINE_LOCATION
) == PackageManager.PERMISSION_GRANTED

fun requestLocationPermission(activity: Activity, requestCode: Int) =
    ActivityCompat.requestPermissions(
        activity,
        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
        requestCode
    )

fun isLocationServiceEnabled(context: Context) =
    LocationManagerCompat.isLocationEnabled(context.getSystemService(Context.LOCATION_SERVICE) as LocationManager)

fun requestRecordAudioPermission(activity: Activity, requestCode: Int) =
    ActivityCompat.requestPermissions(
        activity,
        arrayOf(Manifest.permission.RECORD_AUDIO),
        requestCode
    )

fun isRecordAudioPermissionGranted(context: Context) = ActivityCompat.checkSelfPermission(
    context,
    Manifest.permission.RECORD_AUDIO
) == PackageManager.PERMISSION_GRANTED

fun isDrawOverOtherGranted(context: Context): Boolean? =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        Settings.canDrawOverlays(context)
    } else {
        null
    }

fun isDrawOverOtherNeeded() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q

fun requestDrawOverOtherApps(context: Context) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        context.startActivity(
            Intent(
                Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + context.packageName)
            )
        )
    }
}

fun openAppSettings(context: Context) {
    context.startActivity(Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
        data = Uri.parse("package:${context.packageName}")
    })
}

fun Any?.toJson(): String? = if (this.isNull()) null else Gson().toJson(this)

inline fun <reified T : Any> String.fromJson(): T? =
    if (this.isEmpty()) null else Gson().fromJson(this, T::class.java)

fun String.replaceN() = this.replace("\\n", "\n")