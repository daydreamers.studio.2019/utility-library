package com.daydreamersteam.utility.utils

import android.content.Context

class SharedRefsUtils {
    companion object {
        fun getSharedRef(context: Context) =
            context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)

        fun putBoolean(context: Context, key: String, value: Boolean) =
            getSharedRef(context).edit().let {
                it.putBoolean(key, value)
                it.apply()
            }

        fun putFloat(context: Context, key: String, value: Float) =
            getSharedRef(context).edit().let {
                it.putFloat(key, value)
                it.apply()
            }

        fun putInt(context: Context, key: String, value: Int) =
            getSharedRef(context).edit().let {
                it.putInt(key, value)
                it.apply()
            }

        fun putLong(context: Context, key: String, value: Long) =
            getSharedRef(context).edit().let {
                it.putLong(key, value)
                it.apply()
            }

        fun putString(context: Context, key: String, value: String) =
            getSharedRef(context).edit().let {
                it.putString(key, value)
                it.apply()
            }

        fun getBoolean(context: Context, key: String, default: Boolean): Boolean =
            getSharedRef(context).getBoolean(key, default)

        fun getFloat(context: Context, key: String, default: Float): Float =
            getSharedRef(context).getFloat(key, default)

        fun getLong(context: Context, key: String, default: Long): Long =
            getSharedRef(context).getLong(key, default)

        fun getInt(context: Context, key: String, default: Int): Int =
            getSharedRef(context).getInt(key, default)

        fun getString(context: Context, key: String, default: String): String =
            getSharedRef(context).getString(key, default)!!
    }
}