package com.daydreamersteam.utility.dialogs

import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.daydreamersteam.utility.R


abstract class BaseDialogFragment : DialogFragment() {

    fun getBuilder() = AlertDialog.Builder(requireContext(), R.style.AlertDialog)
}