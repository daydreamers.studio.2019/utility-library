package com.daydreamersteam.utility.dialogs

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import com.daydreamersteam.utility.R
import com.daydreamersteam.utility.databinding.DialogSimpleBinding
import com.daydreamersteam.utility.utils.click
import kotlinx.android.synthetic.main.dialog_simple.view.*

class SimpleDialog : BaseDialogFragment() {
    private lateinit var binding: DialogSimpleBinding
    private var positiveClick: (() -> Unit)? = null
    private var negativeClick: (() -> Unit)? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return getBuilder().setView(
            DataBindingUtil.inflate<DialogSimpleBinding>(
                LayoutInflater.from(requireContext()),
                R.layout.dialog_simple,
                null,
                false
            ).also {
                binding = it
                it.lifecycleOwner = this@SimpleDialog
                binding.title = requireArguments().getString("title")
                binding.content = requireArguments().getString("content")
                binding.positiveText = requireArguments().getString("positiveText")
                binding.negativeText = requireArguments().getString("negativeText")
            }.root.also {
                it.simple_dialog_negative.click {
                    negativeClick?.invoke()
                    dismiss()
                }
                it.simple_dialog_positive.click {
                    positiveClick?.invoke()
                    dismiss()
                }
            }
        ).create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = requireArguments().getBoolean("cancelable")
    }

    companion object {
        const val TAG = "SimpleDialog"
        fun getInstance(
            title: String?,
            content: String?,
            cancelable: Boolean,
            positiveText: String,
            positiveClick: () -> Unit,
            negativeText: String? = null,
            negativeClick: (() -> Unit)? = null
        ) =
            SimpleDialog().apply {
                this.positiveClick = positiveClick
                this.negativeClick = negativeClick
                arguments = Bundle().apply {
                    putString("title", title)
                    putString("content", content)
                    putString("positiveText", positiveText)
                    putString("negativeText", negativeText)
                    putBoolean("cancelable", cancelable)
                }
            }
    }
}