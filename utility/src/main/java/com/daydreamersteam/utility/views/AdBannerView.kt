package com.daydreamersteam.utility.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import com.daydreamersteam.utility.R
import com.daydreamersteam.utility.databinding.AdBannerViewBinding
import com.daydreamersteam.utility.utils.logd
import com.daydreamersteam.utility.utils.loge
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.ad_banner_view.view.*

class AdBannerView : ConstraintLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    private val binding: AdBannerViewBinding = DataBindingUtil.inflate(
        LayoutInflater.from(context),
        R.layout.ad_banner_view,
        this,
        true
    )

    companion object {
        const val TAG = "AdBannerView"

        @BindingAdapter("tools:adSize", "tools:adUnitId")
        @JvmStatic
        fun applyAttrs(adBannerView: AdBannerView, adSize: String, adUnitId: String) {
            AdView(adBannerView.context).apply {
                adListener = object : AdListener() {
                    override fun onAdFailedToLoad(p0: Int) {
                        super.onAdFailedToLoad(p0)
                        loge(TAG, "onAdFailedToLoad $p0")
                    }

                    override fun onAdLoaded() {
                        super.onAdLoaded()
                        logd(TAG, "onAdLoaded")
                        adBannerView.ad_banner_view_main_layout.removeView(this@apply)
                        adBannerView.ad_banner_view_main_layout.addView(
                            this@apply,
                            LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT
                        )
                    }
                }

                this.adSize = when (adSize) {
                    "BANNER" -> AdSize.BANNER
                    "SMART_BANNER" -> AdSize.SMART_BANNER
                    "LARGE_BANNER" -> AdSize.LARGE_BANNER
                    "MEDIUM_RECTANGLE" -> AdSize.MEDIUM_RECTANGLE
                    "FULL_BANNER" -> AdSize.FULL_BANNER
                    "LEADERBOARD" -> AdSize.LEADERBOARD
                    else -> AdSize.SMART_BANNER
                }
                this.adUnitId = adUnitId
                loadAd(AdRequest.Builder().build())
            }
        }
    }
}